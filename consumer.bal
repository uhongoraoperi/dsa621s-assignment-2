import ballerina/log;
import wso2/kafka;


@Description{value : "creating server level annotation."}
@kafka:configuration {
    bootstrapServers: "localhost:9092, localhost:9093",
    groupId: "user",
    topics: ["remoteFile"],
    pollingInterval: 3000,
    autoCommit: true
}
service<kafka> kafkaService {
    resource onMessage (kafka:Consumer consumer, kafka:ConsumerRecord[] records) {
       // record serialization
       int tracker = 0;
       while (tracker < lengthof records ) {
             processKafkaRecord(records[tracker]);
             tracker = tracker + 1;
       }
       // appendind commits
       consumer.commit();
    }
}

function getRequest(kafka:ConsumerRecord record) {
    blob serializedMsg = record.value;
    string msg = serializedMsg.toString("UTF-8");
    // retrievinf file record.
    println("Topic: " + record.topic + " Received Message: " + msg);
}
