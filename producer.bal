import ballerina/log;
import wso2/kafka;

function main (string[] args) {
    string msg = "Noficaton received from remote server";
    blob serializedMsg = msg.toBlob("UTF-8");
    // Generaing a pre-POST record with initial suitable partintions
    kafka:ProducerRecord record = { value:serializedMsg, topic:"Distributed File System", partition:3 };
        kafka:ProducerConfig producerConfig = { clientID:"basic-producer", acks:"all", noRetries:3};
  fileGenerato(record, producerConfig);
}

function fileGenerator(kafka:ProducerRecord record, kafka:ProducerConfig producerConfig) {
    endpoint<kafka:ProducerClient> kafkaEP {
        create kafka:ProducerClient (["localhost:9092, localhost:9093"], producerConfig);
    }
    kafkaEP.sendAdvanced(record);
    kafkaEP.flush();
    kafkaEP.close();
}